package com.smh.satellite.domain

import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.model.position.PositionItem
import com.smh.satellite.data.repository.SatelliteRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPositionsByIdUseCaseImpl @Inject constructor(private val satelliteRepository: SatelliteRepository) :
    GetPositionsByIdUseCase {
    override fun execute(id: Long) = satelliteRepository.getPositionItemById(id)
}

interface GetPositionsByIdUseCase {
    fun execute(id: Long): Flow<Resource<PositionItem>>
}