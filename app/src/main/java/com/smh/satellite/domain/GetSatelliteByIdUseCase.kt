package com.smh.satellite.domain

import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.repository.SatelliteRepository
import com.smh.satellite.data.source.localSource.db.entity.SatelliteDetailEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetSatelliteByIdUseCaseImpl @Inject constructor(private val satelliteRepository: SatelliteRepository) :
    GetSatelliteByIdUseCase {
    override fun execute(id: Long) = satelliteRepository.getSatelliteById(id)
}

interface GetSatelliteByIdUseCase {
    fun execute(id:Long): Flow<Resource<SatelliteDetailEntity>>
}