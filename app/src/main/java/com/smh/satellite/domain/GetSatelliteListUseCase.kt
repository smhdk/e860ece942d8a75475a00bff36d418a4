package com.smh.satellite.domain

import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.model.list.SatelliteItem
import com.smh.satellite.data.repository.SatelliteRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetSatelliteListUseCaseImpl @Inject constructor(private val satelliteRepository: SatelliteRepository) :
    GetSatelliteListUseCase {

    override fun execute(): Flow<Resource<List<SatelliteItem>>> {
        return satelliteRepository.getSatelliteList()
    }
}

interface GetSatelliteListUseCase {
    fun execute(): Flow<Resource<List<SatelliteItem>>>
}