package com.smh.satellite.core.di

import com.smh.satellite.domain.GetPositionsByIdUseCase
import com.smh.satellite.domain.GetPositionsByIdUseCaseImpl
import com.smh.satellite.domain.GetSatelliteByIdUseCase
import com.smh.satellite.domain.GetSatelliteByIdUseCaseImpl
import com.smh.satellite.domain.GetSatelliteListUseCase
import com.smh.satellite.domain.GetSatelliteListUseCaseImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class DomainModule {

    @Singleton
    @Binds
    abstract fun provideGetSatelliteListUseCase(getSatelliteListUseCase: GetSatelliteListUseCaseImpl): GetSatelliteListUseCase

    @Singleton
    @Binds
    abstract fun provideGetSatelliteDetailByIdUseCase(getSatelliteDetailUseCase: GetSatelliteByIdUseCaseImpl): GetSatelliteByIdUseCase

    @Singleton
    @Binds
    abstract fun provideGetPositionsUseCase(getPositionsByIdUseCase: GetPositionsByIdUseCaseImpl): GetPositionsByIdUseCase

}