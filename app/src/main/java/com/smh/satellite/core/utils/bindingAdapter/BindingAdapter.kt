package com.smh.satellite.core.utils.bindingAdapter

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.smh.satellite.core.base.BaseAdapter
import com.smh.satellite.core.base.BaseAdapterItem

@BindingAdapter("setVisibilityByBool")
fun setVisibilityByBool(view: View, bool: Boolean?) {
    if (bool == null || !bool) {
        view.visibility = View.GONE
    } else {
        view.visibility = View.VISIBLE
    }
}

@BindingAdapter("setList")
fun setList(recyclerView: RecyclerView, list: List<BaseAdapterItem>) {
    (recyclerView.adapter as BaseAdapter<BaseAdapterItem>).submitList(list)
}
