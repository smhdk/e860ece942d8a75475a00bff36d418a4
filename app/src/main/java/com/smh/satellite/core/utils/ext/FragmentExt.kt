package com.smh.satellite.core.utils.ext

import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.smh.satellite.core.utils.FragmentViewBindingDelegate

/**
 * A delegation function that initializes [ViewBinding] for desired [Fragment]
 *
 * @param viewBindingFactory The factory to initialize binding
 */
fun <T : ViewBinding> Fragment.viewBinding(
    viewBindingFactory: (View) -> T
) = FragmentViewBindingDelegate(this, viewBindingFactory)