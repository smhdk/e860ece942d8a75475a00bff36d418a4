package com.smh.satellite.core.utils.ext

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

private const val AppDateFormat = "dd.MM.yyyy"
private const val AssetDateFormat = "yyyy-MM-dd"

fun String.toAppDateFormat(): String {
    val appDateFormat = SimpleDateFormat(AppDateFormat, Locale.getDefault())
    val assetDateFormat = SimpleDateFormat(AssetDateFormat, Locale.getDefault())

    val date: Date? = assetDateFormat.parse(this)
    return appDateFormat.format(date ?: Date())
}