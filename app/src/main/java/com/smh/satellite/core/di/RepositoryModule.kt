package com.smh.satellite.core.di

import com.smh.satellite.data.repository.SatelliteRepository
import com.smh.satellite.data.repository.SatelliteRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun provideSatelliteRepository(satelliteRepository: SatelliteRepositoryImpl): SatelliteRepository
}