package com.smh.satellite.core.utils.ext

import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.smh.satellite.R

fun <T : ViewDataBinding?> ViewGroup.bindingInflate(@LayoutRes resourceId: Int): T =
    DataBindingUtil.inflate<T>(
        LayoutInflater.from(context),
        resourceId,
        this,
        false
    )

fun RecyclerView.addItemDecorationWithoutLastItem() {
    if (layoutManager !is LinearLayoutManager)
        return
    addItemDecoration(object : DividerItemDecoration(context, LinearLayoutManager.VERTICAL) {
        private val mDivider: Drawable? = ContextCompat.getDrawable(context, R.drawable.divider)
        override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
            val dividerLeft = parent.paddingLeft
            val dividerRight = parent.width - parent.paddingRight
            val childCount = parent.childCount
            for (i in 0..childCount - 2) {
                val child = parent.getChildAt(i)
                val params = child.layoutParams as RecyclerView.LayoutParams
                val dividerTop = child.bottom + params.bottomMargin
                val dividerBottom = dividerTop + (mDivider?.intrinsicHeight ?: 0)
                mDivider?.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
                mDivider?.draw(c)
            }
        }
    })
}