package com.smh.satellite.core.base

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.viewbinding.ViewBinding
import com.smh.satellite.BR

abstract class BaseFragment<V : ViewBinding>(@LayoutRes layout: Int) : Fragment(layout),
    LifecycleObserver {

    abstract val mBinding: V

    abstract val viewModel: BaseViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (mBinding as ViewDataBinding).lifecycleOwner = viewLifecycleOwner
        (mBinding as ViewDataBinding).setVariable(BR.viewModel, viewModel)

        initView()
    }

    open fun initView() {}

}