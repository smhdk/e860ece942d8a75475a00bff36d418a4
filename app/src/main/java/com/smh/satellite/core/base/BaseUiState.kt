package com.smh.satellite.core.base

interface BaseUiState {
    var errorMessage: String?
    var isLoading: Boolean
}