package com.smh.satellite.core.utils.ext

import java.text.NumberFormat

fun Float.format(): String? {
    return NumberFormat.getNumberInstance().format(this)
}

fun Double.format(): String {
    return NumberFormat.getNumberInstance().format(this)
}