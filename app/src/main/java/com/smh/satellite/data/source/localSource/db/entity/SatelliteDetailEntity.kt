package com.smh.satellite.data.source.localSource.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "satellite_entity")
data class SatelliteDetailEntity(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Long = 0L,
    @SerializedName("cost_per_launch")
    val costPerLaunch: Double = .0,
    @SerializedName("first_flight")
    val firstLaunch: String = "",
    @SerializedName("height")
    val height: Double = .0,
    @SerializedName("mass")
    val mass: Double = .0
)