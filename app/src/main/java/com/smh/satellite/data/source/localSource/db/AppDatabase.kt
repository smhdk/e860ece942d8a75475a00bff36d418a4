package com.smh.satellite.data.source.localSource.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.smh.satellite.data.source.localSource.db.dao.SatelliteDetailDao
import com.smh.satellite.data.source.localSource.db.entity.SatelliteDetailEntity

@Database(
    entities = [
        SatelliteDetailEntity::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun satelliteDao(): SatelliteDetailDao
}