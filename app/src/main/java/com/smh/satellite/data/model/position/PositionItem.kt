package com.smh.satellite.data.model.position

import com.google.gson.annotations.SerializedName


data class PositionItem(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("positions")
    val positions: List<Coordinat> = emptyList()
)