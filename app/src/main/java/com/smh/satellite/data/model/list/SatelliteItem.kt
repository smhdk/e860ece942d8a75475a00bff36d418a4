package com.smh.satellite.data.model.list

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.smh.satellite.core.base.BaseAdapterItem
import kotlinx.parcelize.Parcelize

@Parcelize
data class SatelliteItem(
    @SerializedName("id")
    val id: Long = 0L,
    @SerializedName("active")
    val isActive: Boolean = false,
    @SerializedName("name")
    val name: String = ""
) : Parcelable, BaseAdapterItem