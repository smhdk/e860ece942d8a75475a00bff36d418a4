package com.smh.satellite.data.repository

import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.model.list.SatelliteItem
import com.smh.satellite.data.model.position.PositionItem
import com.smh.satellite.data.source.localSource.db.entity.SatelliteDetailEntity
import kotlinx.coroutines.flow.Flow

interface SatelliteRepository {
    fun getSatelliteList(): Flow<Resource<List<SatelliteItem>>>

    fun getSatelliteById(id: Long): Flow<Resource<SatelliteDetailEntity>>

    fun getPositionItemById(id: Long): Flow<Resource<PositionItem>>
}