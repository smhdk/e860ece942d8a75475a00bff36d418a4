package com.smh.satellite.data.model.position

import com.google.gson.annotations.SerializedName

data class Position(
    @SerializedName("list")
    val list: List<PositionItem>,
)