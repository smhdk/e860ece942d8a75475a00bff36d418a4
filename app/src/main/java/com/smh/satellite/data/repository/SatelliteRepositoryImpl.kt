package com.smh.satellite.data.repository

import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.source.localSource.asset.AssetSource
import com.smh.satellite.data.source.localSource.db.dao.SatelliteDetailDao
import com.smh.satellite.data.source.localSource.db.entity.SatelliteDetailEntity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

class SatelliteRepositoryImpl @Inject constructor(
    private val assetSource: AssetSource,
    private val satelliteDetailDao: SatelliteDetailDao
) : SatelliteRepository {
    override fun getSatelliteList() = assetSource.getSatelliteList()

    override fun getSatelliteById(id: Long) = flow {
        emit(Resource.Loading)
        val cachedSatellite = satelliteDetailDao.getSatelliteDetail(id)
        if (cachedSatellite != null)
            emit(Resource.Success(cachedSatellite))
        else {
            val assetSatelliteResource = assetSource.getSatelliteById(id)
            insertAssetSatelliteToDb(assetSatelliteResource)
            emit(assetSatelliteResource)
        }
    }

    private fun insertAssetSatelliteToDb(assetSatelliteResource: Resource<SatelliteDetailEntity>) {
        when (assetSatelliteResource) {
            is Resource.Success -> GlobalScope.launch {
                satelliteDetailDao.insertSatelliteDetail(assetSatelliteResource.data)
            }

            else -> {}
        }
    }

    override fun getPositionItemById(id: Long) = assetSource.getPositionById(id)
}