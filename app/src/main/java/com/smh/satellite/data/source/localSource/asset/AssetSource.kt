package com.smh.satellite.data.source.localSource.asset

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.model.list.SatelliteItem
import com.smh.satellite.data.model.position.Position
import com.smh.satellite.data.source.localSource.db.entity.SatelliteDetailEntity
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException
import javax.inject.Inject

class AssetSource @Inject constructor(@ApplicationContext private val context: Context) {

    fun getSatelliteList(): Flow<Resource<List<SatelliteItem>>> = flow {
        emit(Resource.Loading)
        try {
            val listString = context.assets.open(LIST_FILE_NAME)
                .bufferedReader()
                .use { it.readText() }
            val type = object : TypeToken<List<SatelliteItem>>() {}.type
            val list = Gson().fromJson<List<SatelliteItem>>(listString, type)
            emit(Resource.Success(list))
        } catch (ioException: IOException) {
            emit(Resource.Error(ioException.message))
        }
    }

    fun getSatelliteById(id: Long): Resource<SatelliteDetailEntity> =
        try {
            val listString = context.assets.open(DETAIL_FILE_NAME)
                .bufferedReader()
                .use { it.readText() }
            val type = object : TypeToken<List<SatelliteDetailEntity>>() {}.type
            val list = Gson().fromJson<List<SatelliteDetailEntity>>(listString, type)
            Resource.Success(list.find { it.id == id })
        } catch (ioException: IOException) {
            Resource.Error(ioException.message)
        }


    fun getPositionById(id: Long) = flow {
        emit(Resource.Loading)
        try {
            val listString = context.assets.open(POSITION_FILE_NAME)
                .bufferedReader()
                .use { it.readText() }
            val type = object : TypeToken<Position>() {}.type
            val positionModel = Gson().fromJson<Position>(listString, type)
            val positionItem = positionModel.list.find { it.id == id.toString() }
            emit(Resource.Success(positionItem))
        } catch (ioException: IOException) {
            emit(Resource.Error(ioException.message))
        }
    }

    companion object {
        private const val LIST_FILE_NAME = "list.json"
        private const val DETAIL_FILE_NAME = "details.json"
        private const val POSITION_FILE_NAME = "positions.json"
    }
}