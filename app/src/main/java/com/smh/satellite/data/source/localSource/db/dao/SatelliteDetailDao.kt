package com.smh.satellite.data.source.localSource.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.smh.satellite.data.source.localSource.db.entity.SatelliteDetailEntity

@Dao
interface SatelliteDetailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSatelliteDetail(satelliteEntity: SatelliteDetailEntity)

    @Query("Select * from satellite_entity  where id = :id")
    fun getSatelliteDetail(id: Long): SatelliteDetailEntity?
}
