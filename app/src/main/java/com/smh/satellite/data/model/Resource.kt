package com.smh.satellite.data.model

sealed class Resource<in T> {
    data class Success<T>(val data: T) : Resource<T>()
    data class Error(val message: String?, val errorCode: Int = 0) : Resource<Any>()
    object Loading : Resource<Any>()
}
