package com.smh.satellite.data.model.position

import com.google.gson.annotations.SerializedName

data class Coordinat(
    @SerializedName("posX")
    val posX: Double = 0.0,
    @SerializedName("posY")
    val posY: Double = 0.0
)