package com.smh.satellite.ui.list

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.smh.satellite.R
import com.smh.satellite.core.base.BaseAdapter
import com.smh.satellite.data.model.list.SatelliteItem
import com.smh.satellite.databinding.ItemSatelliteBinding
import com.smh.satellite.core.utils.ext.bindingInflate

class SatelliteAdapter(private val onClick: (SatelliteItem?) -> Unit) : BaseAdapter<SatelliteItem>() {
    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        val mBinding = parent.bindingInflate<ItemSatelliteBinding>(R.layout.item_satellite)
        mBinding.root.setOnClickListener {
            onClick.invoke(mBinding.item)
        }
        return mBinding
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        val item = getItem(position)
        (binding as ItemSatelliteBinding).item = item

        binding.viewBadge.setBackgroundResource(if (item.isActive) R.drawable.bg_active else R.drawable.bg_passive)
        binding.tvStatus.text = if (item.isActive) binding.tvStatus.context.getString(R.string.active) else binding.tvStatus.context.getString(
                R.string.passive
            )
    }
}