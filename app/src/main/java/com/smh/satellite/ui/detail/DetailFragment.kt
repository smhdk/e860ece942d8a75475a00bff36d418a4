package com.smh.satellite.ui.detail

import androidx.fragment.app.viewModels
import com.smh.satellite.R
import com.smh.satellite.core.base.BaseFragment
import com.smh.satellite.databinding.FragmentDetailBinding
import com.smh.satellite.core.utils.ext.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : BaseFragment<FragmentDetailBinding>(R.layout.fragment_detail) {
    override val viewModel: DetailViewModel by viewModels()
    override val mBinding by viewBinding(FragmentDetailBinding::bind)

}