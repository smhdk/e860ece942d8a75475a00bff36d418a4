package com.smh.satellite.ui.list

import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.smh.satellite.R
import com.smh.satellite.core.base.BaseFragment
import com.smh.satellite.databinding.FragmentListBinding
import com.smh.satellite.core.utils.ext.addItemDecorationWithoutLastItem
import com.smh.satellite.core.utils.ext.viewBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ListFragment : BaseFragment<FragmentListBinding>(R.layout.fragment_list) {
    override val viewModel: ListViewModel by viewModels()
    override val mBinding by viewBinding(FragmentListBinding::bind)
    private val adapter by lazy {
        SatelliteAdapter {
            findNavController().navigate(ListFragmentDirections.actionListToDetail(it))
        }
    }

    override fun initView() {
        super.initView()

        initAdapter()
    }

    private fun initAdapter() {
        mBinding.rvSatellite.adapter = adapter
        mBinding.rvSatellite.addItemDecorationWithoutLastItem()
    }
}