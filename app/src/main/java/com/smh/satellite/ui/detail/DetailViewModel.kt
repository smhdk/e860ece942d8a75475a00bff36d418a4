package com.smh.satellite.ui.detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.smh.satellite.core.base.BaseUiState
import com.smh.satellite.core.base.BaseViewModel
import com.smh.satellite.core.utils.ext.format
import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.model.list.SatelliteItem
import com.smh.satellite.data.model.position.Coordinat
import com.smh.satellite.data.source.localSource.db.entity.SatelliteDetailEntity
import com.smh.satellite.domain.GetPositionsByIdUseCase
import com.smh.satellite.domain.GetSatelliteByIdUseCase
import com.smh.satellite.core.utils.ext.toAppDateFormat
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.text.NumberFormat
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val getSatelliteByIdUseCase: GetSatelliteByIdUseCase,
    private val getPositionsByIdUseCase: GetPositionsByIdUseCase,
    savedStateHandle: SavedStateHandle
) :
    BaseViewModel() {

    private val satelliteItem =
        savedStateHandle.get<SatelliteItem>("satellite_item") ?: SatelliteItem()
    private val satelliteDetailResource =
        MutableStateFlow<Resource<SatelliteDetailEntity>>(Resource.Loading)
    private val positions = MutableStateFlow<List<Coordinat>>(emptyList())

    private val _currentPosition = MutableStateFlow(Coordinat())
    val currentPosition = _currentPosition.asStateFlow()

    val uiState =
        combine(
            satelliteDetailResource,
            currentPosition
        ) { detailResource, currentCoordinat ->
            when (detailResource) {
                is Resource.Error -> DetailUiState(errorMessage = detailResource.message)
                is Resource.Loading -> DetailUiState(isLoading = true)
                is Resource.Success -> DetailUiState(
                    satelliteItem = satelliteItem,
                    satelliteDetail = detailResource.data,
                    costText = detailResource.data.costPerLaunch.format(),
                    heightMassTest = "${detailResource.data.height.format()}/${detailResource.data.mass.format()}",
                    dateText = detailResource.data.firstLaunch.toAppDateFormat(),
                    currentPositionText = " (${currentCoordinat.posX},${currentCoordinat.posY})"
                )
            }
        }.stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(),
            initialValue = DetailUiState(),
        )

    init {
        initPositions()
        initSatelliteDetail()
        changePositions()
    }

    fun initSatelliteDetail() {
        viewModelScope.launch(Dispatchers.IO) {
            getSatelliteByIdUseCase.execute(satelliteItem.id)
                .collectLatest {
                    satelliteDetailResource.value = it
                }
        }
    }

    fun initPositions() {
        viewModelScope.launch {
            getPositionsByIdUseCase.execute(satelliteItem.id)
                .collectLatest {
                    if (it is Resource.Success) {
                        positions.value = it.data.positions
                        _currentPosition.value = it.data.positions.firstOrNull() ?: Coordinat()
                    }
                }
        }
    }

    private fun changePositions() {
        viewModelScope.launch {
            positions.collectLatest { list ->
                (1..Long.MAX_VALUE)
                    .asFlow()
                    .onEach {
                        delay(3000)
                        val index = it % list.size
                        _currentPosition.value = list[index.toInt()]
                    }
                    .collect()
            }
        }
    }
}

data class DetailUiState(
    val satelliteItem: SatelliteItem = SatelliteItem(),
    val satelliteDetail: SatelliteDetailEntity = SatelliteDetailEntity(),
    val heightMassTest: String = "",
    val costText: String = "",
    val dateText: String = "",
    val currentPositionText: String = "",
    override var errorMessage: String? = "",
    override var isLoading: Boolean = false
) : BaseUiState