package com.smh.satellite.ui.list

import androidx.lifecycle.viewModelScope
import com.smh.satellite.core.base.BaseUiState
import com.smh.satellite.core.base.BaseViewModel
import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.model.list.SatelliteItem
import com.smh.satellite.domain.GetSatelliteListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(private val getSatelliteListUseCase: GetSatelliteListUseCase) :
    BaseViewModel() {

    val searchQuery = MutableStateFlow("")
    private val listResource: MutableStateFlow<Resource<List<SatelliteItem>>> =
        MutableStateFlow(Resource.Loading)

    val uiState = combine(listResource, searchQuery) { listResource, query ->
        when (listResource) {
            is Resource.Error -> ListUiState(errorMessage = listResource.message)
            is Resource.Loading -> ListUiState(isLoading = true)
            is Resource.Success -> ListUiState(list = listResource.data
                .filter {
                    if (query.length > 2)
                        it.name.lowercase().contains(query.lowercase())
                    else true
                })
        }
    }.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(),
        initialValue = ListUiState()
    )

    init {
        initList()
    }

    fun initList() {
        viewModelScope.launch {
            getSatelliteListUseCase.execute().collectLatest {
                listResource.value = it
            }
        }
    }
}

class ListUiState(
    val list: List<SatelliteItem> = emptyList(),
    override var errorMessage: String? = "",
    override var isLoading: Boolean = false
) : BaseUiState