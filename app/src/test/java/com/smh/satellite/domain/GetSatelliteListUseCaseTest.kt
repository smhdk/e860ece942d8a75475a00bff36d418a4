package com.smh.satellite.domain

import com.smh.satellite.data.model.Resource
import com.smh.satellite.utils.FakeSatelliteRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

class GetSatelliteListUseCaseTest {

    private lateinit var getSatelliteListUseCase: GetSatelliteListUseCase

    @Mock
    private lateinit var fakeSatelliteRepository: FakeSatelliteRepository

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        getSatelliteListUseCase = GetSatelliteListUseCaseImpl(fakeSatelliteRepository)
    }


    @Test
    fun `Get satellitelist, error return`(): Unit = runBlocking {
        whenever(fakeSatelliteRepository.getSatelliteList())
            .thenReturn(flowOf(Resource.Error("")))

        val satelliteListResource = getSatelliteListUseCase.execute().first()
        assert(satelliteListResource is Resource.Error)
    }

    @Test
    fun `Get satellitelist, success return`(): Unit = runBlocking {
        whenever(fakeSatelliteRepository.getSatelliteList())
            .thenReturn(flowOf(Resource.Success(listOf())))

        val satelliteListResource = getSatelliteListUseCase.execute().first()
        assert(satelliteListResource is Resource.Success<*>)
    }
}
