package com.smh.satellite.domain

import com.smh.satellite.data.model.Resource
import com.smh.satellite.utils.FakeData
import com.smh.satellite.utils.FakeSatelliteRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

class GetPositionsByIdUseCaseTest {

    private lateinit var getPositionsByIdUseCase: GetPositionsByIdUseCase

    @Mock
    private lateinit var fakeSatelliteRepository: FakeSatelliteRepository

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        getPositionsByIdUseCase = GetPositionsByIdUseCaseImpl(fakeSatelliteRepository)
    }


    @Test
    fun `Get position, error return`(): Unit = runBlocking {
        whenever(fakeSatelliteRepository.getPositionItemById(0))
            .thenReturn(flowOf(FakeData.error))

        val satelliteListResource = getPositionsByIdUseCase.execute(0).first()
        assert(satelliteListResource is Resource.Error)
    }

    @Test
    fun `Get position, success return`(): Unit = runBlocking {
        whenever(fakeSatelliteRepository.getPositionItemById(0))
            .thenReturn(flowOf(FakeData.positionItem))

        val satelliteListResource = getPositionsByIdUseCase.execute(0).first()
        assert(satelliteListResource is Resource.Success<*>)
    }
}
