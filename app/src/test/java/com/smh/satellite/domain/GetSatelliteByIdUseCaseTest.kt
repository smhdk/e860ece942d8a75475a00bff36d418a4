package com.smh.satellite.domain

import com.smh.satellite.data.model.Resource
import com.smh.satellite.utils.FakeData.error
import com.smh.satellite.utils.FakeData.satelliteDetail
import com.smh.satellite.utils.FakeSatelliteRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

class GetSatelliteByIdUseCaseTest {

    private lateinit var getSatelliteByIdUseCase: GetSatelliteByIdUseCase

    @Mock
    private lateinit var fakeSatelliteRepository: FakeSatelliteRepository

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        getSatelliteByIdUseCase = GetSatelliteByIdUseCaseImpl(fakeSatelliteRepository)
    }


    @Test
    fun `Get satelliteDetail, error return`(): Unit = runBlocking {
        whenever(fakeSatelliteRepository.getSatelliteById(0))
            .thenReturn(flowOf(error))

        val satelliteListResource = getSatelliteByIdUseCase.execute(0).first()
        assert(satelliteListResource is Resource.Error)
    }

    @Test
    fun `Get satelliteDetail, success return`(): Unit = runBlocking {
        whenever(fakeSatelliteRepository.getSatelliteById(0))
            .thenReturn(flowOf(satelliteDetail))

        val satelliteListResource = getSatelliteByIdUseCase.execute(0).first()
        assert(satelliteListResource is Resource.Success<*>)
    }
}
