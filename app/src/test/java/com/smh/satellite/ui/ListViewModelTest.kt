package com.smh.satellite.ui

import app.cash.turbine.test
import com.smh.satellite.utils.FakeData.loading
import com.smh.satellite.utils.FakeData.satelliteList
import com.smh.satellite.domain.GetSatelliteListUseCase
import com.smh.satellite.ui.list.ListViewModel
import com.smh.satellite.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

class ListViewModelTest {

    private lateinit var viewModel: ListViewModel

    @Mock
    private lateinit var useCase: GetSatelliteListUseCase

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        viewModel = ListViewModel(useCase)
    }

    @Test
    fun `Get satellitelist, loading return`() = runBlocking {
        whenever(useCase.execute()).thenReturn(flowOf(loading))

        viewModel.initList()

        viewModel.uiState.test {
            assertEquals(true, awaitItem().isLoading)
        }
    }

    @Test
    fun search() = runBlocking {
        whenever(useCase.execute()).thenReturn(flowOf(satelliteList))

        val query = "Satellite 1"

        viewModel.initList()
        viewModel.searchQuery.value = query

        viewModel.uiState.test {
            assertEquals(
                satelliteList.data.filter {
                    it.name.lowercase().contains(query.lowercase())
                }.size,
                awaitItem().list.filter {
                    it.name.lowercase().contains(query.lowercase())
                }.size
            )
        }

    }

}

