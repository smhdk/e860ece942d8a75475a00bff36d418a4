package com.smh.satellite.ui

import androidx.lifecycle.SavedStateHandle
import app.cash.turbine.test
import com.smh.satellite.data.model.list.SatelliteItem
import com.smh.satellite.domain.GetPositionsByIdUseCase
import com.smh.satellite.domain.GetSatelliteByIdUseCase
import com.smh.satellite.ui.detail.DetailViewModel
import com.smh.satellite.utils.FakeData.loading
import com.smh.satellite.utils.FakeData.positionItem
import com.smh.satellite.utils.FakeData.satelliteDetail
import com.smh.satellite.utils.MainDispatcherRule
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever

class DetailViewModelTest {

    private lateinit var viewModel: DetailViewModel

    @Mock
    private lateinit var detailUseCase: GetSatelliteByIdUseCase

    @Mock
    private lateinit var positionUseCase: GetPositionsByIdUseCase

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        val savedStateHandle = SavedStateHandle().apply {
            set("satellite_item", SatelliteItem(0))
        }
        viewModel = DetailViewModel(detailUseCase, positionUseCase, savedStateHandle)
    }

    @Test
    fun `Get satellite detail, loading return`() = runBlocking {
        whenever(detailUseCase.execute(0)).thenReturn(flowOf(loading))

        viewModel.initSatelliteDetail()

        viewModel.uiState.test {
            assertEquals(true, awaitItem().isLoading)
        }
    }

    @Test
    fun `Get satellite detail, success return`() = runBlocking {
        whenever(detailUseCase.execute(0)).thenReturn(flowOf(satelliteDetail))

        viewModel.initSatelliteDetail()

        viewModel.uiState.test {
            assertEquals(satelliteDetail.data.id, awaitItem().satelliteDetail.id)
        }
    }

    @Test
    fun `Get positions, success return`() = runBlocking {
        whenever(positionUseCase.execute(0)).thenReturn(flowOf(positionItem))

        viewModel.initPositions()

        viewModel.currentPosition.test {
            assertTrue(awaitItem().posX != 0.0)
        }
    }
}