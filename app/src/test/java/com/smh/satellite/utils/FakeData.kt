package com.smh.satellite.utils

import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.model.list.SatelliteItem
import com.smh.satellite.data.model.position.Coordinat
import com.smh.satellite.data.model.position.PositionItem
import com.smh.satellite.data.source.localSource.db.entity.SatelliteDetailEntity

object FakeData {
    val error = Resource.Error("error_message")
    val loading = Resource.Loading
    val satelliteList = Resource.Success(
        listOf(
            SatelliteItem(0, true, "Satellite 0"),
            SatelliteItem(1, true, "Satellite 1")
        )
    )
    val satelliteDetail = Resource.Success(SatelliteDetailEntity(id = 0))
    val positionItem = Resource.Success(
        PositionItem(
            id = 0,
            positions = listOf(
                Coordinat(0.55, 0.56),
                Coordinat(0.55, 0.56),
                Coordinat(0.55, 0.56),
                Coordinat(0.55, 0.56)
            )
        )
    )
}