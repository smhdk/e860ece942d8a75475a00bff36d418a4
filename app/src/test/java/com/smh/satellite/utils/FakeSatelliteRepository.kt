package com.smh.satellite.utils

import com.smh.satellite.data.model.Resource
import com.smh.satellite.data.model.list.SatelliteItem
import com.smh.satellite.data.model.position.PositionItem
import com.smh.satellite.data.repository.SatelliteRepository
import com.smh.satellite.data.source.localSource.db.entity.SatelliteDetailEntity
import com.smh.satellite.utils.FakeData.positionItem
import com.smh.satellite.utils.FakeData.satelliteDetail
import com.smh.satellite.utils.FakeData.satelliteList
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

open class FakeSatelliteRepository : SatelliteRepository {
    override fun getSatelliteList(): Flow<Resource<List<SatelliteItem>>> = flowOf(satelliteList)

    override fun getSatelliteById(id: Long): Flow<Resource<SatelliteDetailEntity>> = flowOf(
        satelliteDetail
    )

    override fun getPositionItemById(id: Long): Flow<Resource<PositionItem>> = flowOf(
        positionItem
    )
}